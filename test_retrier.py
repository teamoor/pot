from bot import with_retry


def test_with_retry(mocker):
	func = mocker.Mock(side_effect=[ValueError(1), ValueError(2), None]) 
	job_queue = mocker.Mock()
	job_queue.run_once.side_effect = lambda what, when: what()
	with_retry(job_queue, timeout=0, give_up_after=1)(func)()
	assert len(job_queue.run_once.mock_calls) == 2
	assert len(func.mock_calls) == 3
