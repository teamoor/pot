import logging
import pytest
from bot import load_plugins

log = logging.getLogger(__name__)

@pytest.fixture
def plugins():
	return load_plugins()


def test_skip_filter(mocker, plugins):
	log.info(f'Plugins: {plugins}')
	plugin = plugins['filters']['subject_skip_marker']
	context = mocker.Mock()
	mail = mocker.Mock()
	mail.subject = '[foo] [SKIP] SOME-KEY'
	context.bot_data = dict(skip={'@sometarget': ['SOME-KEY']})
	assert plugin(context, mail, '@sometarget')
	mail.subject = '[foo] [SKIP]'
	assert not plugin(context, mail, '@sometarget')
	context.bot_data = dict(skip={'@sometarget': ['__ALL__']})
	mail.subject = '[foo] [SKIP] foo bar'
	assert plugin(context, mail, '@sometarget')
	mail.subject = '[foo] [SKIP]'
	assert plugin(context, mail, '@sometarget')

