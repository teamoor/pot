#!/usr/bin/env python3
import logging
import os
import sys
import poplib
import traceback
import time
from functools import partial, wraps
from pyzmail import PyzMessage, decode_text
from telegram import ParseMode
from telegram.constants import MAX_MESSAGE_LENGTH
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    PicklePersistence,
)
from telegram.ext.jobqueue import Job



logging.basicConfig(format='%(asctime)s %(levelname)s %(name)s '
                           ': %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)


plugins = dict(
    filters=dict(),
)


def entry_points(group):
    try:
        from importlib import metadata
        return metadata.entry_points().get(group, [])
    except ImportError:
        import pkg_resources
        return pkg_resources.iter_entry_points(group)


def load_plugins():
    plugins['filters'][subject_skip_marker.__name__] = subject_skip_marker
    for name, ep in entry_points('pot.filters'):
        add_filter(name, ep.load()) 
    return plugins


def subject_skip_marker(context, mail, target):
    skip = '[SKIP]' in (mail.subject or '')
    if not skip:
        return False
    skip_key = mail.subject.split('[SKIP] ')[-1]
    if not skip_key:
        return False
    skip_settings = context.bot_data.get('skip', {})
    if not skip_settings:
        return False

    if (skip and target in skip_settings and
        skip_key in skip_settings[target]
        or '__ALL__' in skip_settings[target]):
        log.info(f'Skipping {skip_key} in {mail.subject} for {target}')
        return True

    return False


def add_filter(name, callback):
    print(plugins)
    plugins['filters'][name] = callback


class State:
    defaults = dict(
        retry_timeout=12,
        mail_fetch_period=60,
        give_up_after=300,
        owners=tuple(),
    )

    def __init__(self):
        vars(self).update(self.defaults)

state = State()


class Retrier:
    defaults = dict(
        give_up_after=300,
    )

    def __init__(
        self,
        callback,
        job_queue,
        timeout=12,
        max_attempts=0,
        backoff=True,
        give_up_after=None
    ):
        self.callback = callback
        self.timeout = timeout
        self.max_attempts = max_attempts
        self.backoff = backoff
        self.give_up_after = (
            (give_up_after or timeout * 100)
            or
            self.defaults['give_up_after']
        )
        self.retries = 0
        self.started_at = None
        self.job_queue = job_queue

    def __call__(self, *args, **kw):
        if not self.started_at:
            self.started_at = time.time()
        try:
            return self.callback(*args, **kw)
        except Exception as e:
            log.error(f'{self.callback}: {e}')
            self.schedule_retry(*args, **kw)
            self.retries += 1
        else:
            self.retries = 0 
            self.started_at = None

    def schedule_retry(self, *args, **kw):
        if self.max_attempts > 0 and self.max_attempts == self.retries: 
            log.debug(
                f'Giving up after {self.retries}/{self.max_attempts} retries'
            )
            return
        seconds_since_start = time.time() - self.started_at 
        if self.started_at and seconds_since_start >= self.give_up_after:
            log.debug(
                f'Giving up after {seconds_since_start:.2f}s'
            )
            return

        when = self.timeout + (self.retries if self.backoff else 0) * self.timeout / 2 
        log.debug(f'Scheduling retry after {when}s')
        self.job_queue.run_once(
            lambda: self(*args, **kw),
            when,
        )


def with_retry(*args, **kw):
    '''with_retry(job_queue)(callable)(*args, *kw)'''
    def decorator(func):
        retrier = Retrier(func, *args, **kw)
        @wraps(func)
        def wrapper(*args, **kw):
            log.debug(f'calling wrapped {func}')
            return retrier(*args, **kw)
        return wrapper
    return decorator


def allow_users(owners):
    def decorator(func):
        @wraps(func)
        def wrapper(update, context, *args, **kw):
            chat = update.effective_chat
            if not chat:
                log.error(f'No effective_chat in {update}')
                return
            if chat.id not in (owners if not callable(owners) else owners()):
                log.error(f'Not from an owner: {update}')
                return
            return func(update, context, *args, **kw)
        return wrapper
    return decorator


def get_owners():
    return state.owners


class EmailClient(object):
    def __init__(self, username, password, host=None):
        self.username = username
        self.password = password
        self.host = host
        self.server = self.connect()

    def connect(self):
        pop3_server = self.host or f"pop.{self.username.split('@')[-1]}"
        server = poplib.POP3_SSL(pop3_server)
        log.info(server.getwelcome().decode('utf8'))
        server.user(self.username)
        server.pass_(self.password)
        return server

    def __iter__(self):
        return self.server.list()[1]

    def __len__(self):
        return len(self.server.list()[1])

    def __getitem__(self, index):
        return Email(self.server.retr(index)[1])

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            log.info('Exited normally\n')
            self.server.quit()
        else:
            log.exception(f"Error: {exc_val}")
            self.server.close()
            return False # Propagate


class Email(object):
    def __init__(self, raw_mail_lines):
        msg_content = b'\r\n'.join(raw_mail_lines)
        msg =  PyzMessage.factory(msg_content)
        self.msg = msg

        self.subject = msg.get_subject()
        self.sender = msg.get_address('from')
        self.date = msg.get_decoded_header('date', '')
        self.id = msg.get_decoded_header('message-id', '')

        for mailpart in msg.mailparts:
            if mailpart.is_body == 'text/plain':
                payload, used_charset = decode_text(
                    mailpart.get_payload(),
                    mailpart.charset,
                    None
                )
                self.charset = used_charset
                self.text = payload
                return
            else:
                self.text = None

    def __repr__(self):
        return (
            f"Date: {self.date}\n"
            f"Subject: {self.subject}\n"
            f"From: {self.sender}\n"
            f"ID: {self.id}\n"
            f"{self.text}\n"
        )

    def __str__(self):
        return self.text or ''



def split_large_message(text):
    while text:
        if len(text) < MAX_MESSAGE_LENGTH:
            yield text
            text = None
        else:
            out = text[:MAX_MESSAGE_LENGTH]
            yield out
            text = text.lstrip(out)


def error(update, context):
    log.error(f'update: {update}\nerror: {context.error}')


def start_callback(update, context):
    if update.message.chat.username not in state.owners:
        return
    msg = "Use /help to get help"
    update.message.reply_text(msg)


@allow_users(get_owners)
def help_(update, context):
    help_str = (
        "*Mailbox Setting*: \n"
        "/setting username password pop3.host.name @channel [@channel…]"
    )
    context.bot.send_message(
        update.message.chat_id, 
        parse_mode=ParseMode.MARKDOWN,
        text=help_str
    )


def target_from_str(arg):
    return arg if not arg.startswith('-') else int(arg) 

@allow_users(get_owners)
def setting_email(update, context):
    log.info(f"/setting {context.args}")
    chat_id = update.message.chat_id
    username = context.args[0]
    password = context.args[1]
    host = context.args[2]
    targets = [
        target_from_str(arg)
        for arg in context.args[3:]
    ]
    key = f'{username}@{host}'
    config = dict(
        username=username,
        password=password,
        host=host,
        chat_id=chat_id,
        key=key,
        targets=targets,
        inbox_num=len(EmailClient(username, password, host))
    )
    context.bot_data[key] = config

    job = context.job_queue.run_repeating(
        check_inbox_job,
        state.mail_fetch_period,
        context=key,
        name=key,
    )
    update.message.reply_text("Configured successfully!")
    log.info(f"Periodic job scheduled: {job}")


@allow_users(get_owners)
def skip(update, context):
    log.info(f"/skip {context.args}")
    chat_id = update.message.chat_id
    context.bot_data.setdefault('skip', {})
    if not context.args:
        update.message.reply_text(f"Skip settings:\n{context.bot_data['skip']}")
        return
    try:
        target = target_from_str(context.args[0])
        skip = context.args[1:]
    except IndexError:
        update.message.reply_text('Usage: /skip [@channel [skip-key …]]')
        return
    context.bot_data['skip'].setdefault(target, tuple(skip))

    update.message.reply_text(
        f"Updated skip settings:\n{context.bot_data['skip']}"
    )


def check_inbox_job(context):
    log.info("Entering periodic task: {context.job}")
    job = context.job
    key = job.context
    bot = context.bot

    data = context.bot_data[key]
    username = data['username']
    password = data['password']
    host = data['host']
    inbox_num = data['inbox_num']
    targets = data['targets']
    log.debug(f"job context: {data}")
    with EmailClient(username, password, host) as client:
        count = len(client)
        if count <= inbox_num:
            log.info('No new mail')
            return
        mails = [
            (i, client[i])
            for i in range(inbox_num + 1, count + 1)
        ]

    for i, mail in mails:
        for target in targets:
            for name, plugin in plugins['filters'].items():
                if plugin(context, mail, target):
                    continue

            log.info(f'Sending {i} ({mail.id}) to {target}') 
            for text in split_large_message(str(mail)):
                with_retry(job.job_queue)(bot.send_message)(
                    chat_id=target,
                    text=text
                )

        data['inbox_num'] = i
        context.bot_data[key] = data
        context.dispatcher.persistence.update_bot_data(context.bot_data)


@allow_users(get_owners)
def inbox(update, context):
    log.info(f"/inbox {context.args}")
    try:
        username = context.args[0] 
        host = context.args[1]
    except IndexError:
        update.message.reply_text('Usage: /inbox username host')
        return

    key = f"{username}@{host}"
    data = context.bot_data[key]
    password = data['password']
    count = len(EmailClient(username, password, host))
    reply_text = (
        f"The index of newest mail is *{count}*," 
        f" received *{count - data['inbox_num']}* new mails since last" 
        f" time you checked."
    )
    with_retry(context.job_queue)(context.bot.send_message)(
        chat_id=update.message.chat_id,
        parse_mode=ParseMode.MARKDOWN,
        text=reply_text,
    )


@allow_users(get_owners)
def get(update, context):
    log.info(f"/get {context.args}")
    try:
        username = context.args[0] 
        host = context.args[1]
        index = int(context.args[2])
    except IndexError:
        update.message.reply_text('Usage: /get username host message-number')
        return

    key = f"{username}@{host}"
    data = context.bot_data[key]
    password = data['password']
    mail = EmailClient(username, password, host)[index]
    for text in split_large_message(repr(mail)):
        with_retry(context.job_queue)(context.bot.send_message)(
            chat_id=update.message.chat_id,
            text=text,
        )


@allow_users(get_owners)
def send(update, context):
    log.info(f"/send {context.args}")
    try:
        username = context.args[0] 
        host = context.args[1]
        index = int(context.args[2])
    except IndexError:
        update.message.reply_text(
            'Usage: /send username host message-number [@channel …]]'
        )
        return

    targets = [ target_from_str(arg) for arg in context.args[3:] ]
    key = f"{username}@{host}"
    data = context.bot_data[key]
    password = data['password']
    mail = EmailClient(username, password, host)[index]
    content = str(mail)
    for target in targets or data.get('targets', []):
        for text in split_large_message(content):
            with_retry(context.job_queue)(context.bot.send_message)(
                chat_id=target,
                text=text
            )


def message(update, context):
    log.debug(f'Message: {update}\n{context}')


def main():
    load_plugins()

    bot_token = os.environ['TELEGRAM_TOKEN']
    state.owners = [ int(i) for i in os.environ["OWNERS"].split() ]
    state.mail_fetch_period = int(os.environ.get(
        "MAIL_FETCH_PERIOD",
        state.defaults['mail_fetch_period']
    ))
    state.retry_timeout = int(os.environ.get(
        "RETRY_TIMEOUT",
        state.defaults['retry_timeout']
    ))

    pers = PicklePersistence(filename=os.environ.get('BOT_DATA', 'bot-data'))
    updater = Updater(token=bot_token, persistence=pers, use_context=True)

    for key, data in pers.get_bot_data().items():
        if 'username' not in data or 'host' not in data:
            continue

        updater.job_queue.run_repeating(
            check_inbox_job,
            state.mail_fetch_period,
            context=key,
            name=key,
        )

    dp = updater.dispatcher
    dp.add_handler(MessageHandler(~Filters.command, message))
    dp.add_handler(CommandHandler("start", start_callback))
    dp.add_handler(CommandHandler("help", help_))
    dp.add_handler(CommandHandler("setting", setting_email))
    dp.add_handler(CommandHandler("inbox", inbox))
    dp.add_handler(CommandHandler("send", send))
    dp.add_handler(CommandHandler("skip", skip))
    dp.add_handler(CommandHandler("get", get))

    dp.add_error_handler(error)

    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
