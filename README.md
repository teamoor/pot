# Usage

## Pre-requisites

1. python >= 3.6
1. pipenv

## Installing

```
git clone https://gitlab.com/teamoor/pot.git
cd  pot
pipenv install
```

## Running with systemd

Prepare systemd env file

```
cat > env<<EOF
TELEGRAM_TOKEN=<YOUR-BOT-TOKEN>
OWNERS=<YOUR-TELEGRAM-NUMERIC-USER-ID> <SOME-OTHER-USER-ID> …
EOF
```

NOTE: To find out your Telegram user ID talk to `@userinfobot`

Create systemd service file

```
sudo cp pot.service.example /etc/systemd/system/pot.service
sudo edit /etc/systemd/system/pot.service
sudo systemctl daemon-reload
sudo systemctl enable pot.service
sudo systemctl start pot.service
sudo systemctl status pot.service
```

